var datatramyte = [
    {
      "STT": 1,
      "Name": "Trạm Y tế Phường Long Thạnh",
      "Latitude":  105.2838511,
      "Rating": 2.98,
      "Longtitude": 10.7708956,
      "address": "Phường Long Thạnh, Thị xã Tân Châu, Tỉnh ",
      "Number_of_beds": "",
      "area": ""
},{
      "STT": 2,
      "Name": "Trạm Y tế Phường Long Hưng",
      "Latitude":  105.2308219,
      "Rating": 3.98,
      "Longtitude": 10.7958634,
      "address": "Phường Long Hưng, Thị xã Tân Châu, Tỉnh ",
      "Number_of_beds": "",
      "area": ""
},{
      "STT": 3,
      "Name": "Trạm Y tế Phường Long Châu",
      "Latitude":   105.2370339,
      "Rating": 4.98,
      "Longtitude": 10.7951531 ,
      "address": "Phường Long Châu, Thị xã Tân Châu, Tỉnh ",
      "Number_of_beds": "",
      "area": ""
},{
      "STT": 4,
      "Name": "Trạm Y tế Xã Phú Lộc",
      "Latitude":   105.1434387,
      "Rating": 1.98,
      "Longtitude": 10.9095149,
      "address": "Xã Phú Lộc, Thị xã Tân Châu, Tỉnh ",
      "Number_of_beds": "",
      "area": ""
},{
      "STT": 5,
      "Name": "Trạm Y tế Xã Vĩnh Xương",
      "Latitude":  105.1668326,
      "Rating": 2.98,
      "Longtitude": 10.8973641,
      "address": "Xã Vĩnh Xương, Thị xã Tân Châu, Tỉnh ",
      "Number_of_beds": "",
      "area": ""
},{
      "STT": 6,
      "Name": "Trạm Y tế Xã Vĩnh Hòa",
      "Latitude":  105.1785307,
      "Rating": 3.98,
      "Longtitude": 10.8528973  ,
      "address": "Xã Vĩnh Hòa, Thị xã Tân Châu, Tỉnh ",
      "Number_of_beds": "",
      "area": ""

},{
      "STT": 7,
      "Name": "Trạm Y tế Xã Tân Thạnh",
      "Latitude": 105.191593,
      "Rating": 4.98,
      "Longtitude": 10.8129361,
      "address": "Xã Tân Thạnh, Thị xã Tân Châu, Tỉnh ",
      "Number_of_beds": "",
      "area": ""

},{
      "STT": 8,
      "Name": "Trạm Y tế Xã Tân An",
      "Latitude": 105.191593,
      "Rating": 4.98,
      "Longtitude": 10.8129361,
      "address": "Xã Tân An, Thị xã Tân Châu, Tỉnh ",
      "Number_of_beds": "",
      "area": ""

},
{
      "STT": 9,
      "Name": "Trạm Y tế Xã Long An",
      "Latitude": 105.1902297,
      "Rating": 5.98,
      "Longtitude": 10.7865361,
      "address": "Xã Long An, Thị xã Tân Châu, Tỉnh ",
      "Number_of_beds": "",
      "area": ""
}
,{
      "STT": 10,
      "Name": "Trạm Y tế Phường Long Phú",
      "Latitude": 105.2241812,
      "Rating": 5.98,
      "Longtitude": 10.78428855,
      "address": "Phường Long Phú, Thị xã Tân Châu, Tỉnh ",
      "Number_of_beds": "",
      "area": ""
}
];
