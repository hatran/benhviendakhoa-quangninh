var data2 = [
 {
   "STT": 1,
   "tencoso": " Viện Huyết học - Truyền máu Trung ương",
   "address": "Phạm Văn Bạch, Yên Hoà, Cầu Giấy, Hà Nội, Vietnam",
   "Longtitude": 21.0248937,
   "Latitude": 105.7862883
 },
 {
   "STT": 2,
   "tencoso": " Viện Vệ sinh dịch tễ Trung ương",
   "address": "131 Lò Đúc, Đống Mác, Hai Bà Trưng, Phạm Đình Hổ Hai Bà Trưng Hà Nội, Vietnam",
   "Longtitude": 21.0129832,
   "Latitude": 105.8567559
 },
 {
   "STT": 3,
   "tencoso": " Viện Vệ sinh dịch tễ Tây Nguyên",
   "address": " 34 Phạm Hùng, phường Tân An,thành phố Buôn Ma Thuột, tỉnh Đăk Lăk",
   "Longtitude": 12.6935746,
   "Latitude": 108.0506829
 },
 {
   "STT": 4,
   "tencoso": " Viện Pasteur Nha Trang",
   "address": "8 Trần Phú, Xương Huân, Tp. Nha Trang, Khánh Hòa 650000, Vietnam",
   "Longtitude": 12.2522988,
   "Latitude": 109.1934974
 },
 {
   "STT": 5,
   "tencoso": " Viện Pasteur thành phố Hồ Chí Minh",
   "address": "167 Pasteur, Phường 8, Quận 3, Hồ Chí Minh, Vietnam",
   "Longtitude": 10.7862408,
   "Latitude": 106.6865762
 },
 {
   "STT": 6,
   "tencoso": " Viện Dinh dưỡng",
   "address": "48B Tăng Bạt Hổ, Phạm Đình Hổ, Hai Bà Trưng, Hà Nội, Vietnam",
   "Longtitude": 21.0135759,
   "Latitude": 105.85678
 },
 {
   "STT": 7,
   "tencoso": " Viện Y học Biển",
   "address": "Đại lộ Võ Nguyên Giáp, Kênh Dương, Lê Chân, Hải Phòng 181050, Vietnam",
   "Longtitude": 20.830418,
   "Latitude": 106.6827664
 },
 {
   "STT": 8,
   "tencoso": " Viện Sức khỏe nghề nghiệp và môi trường",
   "address": "57 Lê Quý Đôn, Phạm Đình Hổ, Hai Bà Trưng, Hà Nội, Vietnam",
   "Longtitude": 21.012618,
   "Latitude": 105.8574827
 },
 {
   "STT": 9,
   "tencoso": " Viện Y tế công cộng thành phố Hồ Chí Minh",
   "address": "bến Nguyễn Duy, Phường 8, Quận 8, Hồ Chí Minh, Vietnam",
   "Longtitude": 10.7492412,
   "Latitude": 106.6759965
 },
 {
   "STT": 10,
   "tencoso": " Viện Sốt rét - Ký sinh trùng - Côn trùng Trung ương",
   "address": "34 Trung Văn, Nam Từ Liêm, Hà Nội, Vietnam",
   "Longtitude": 20.9924215,
   "Latitude": 105.7908032
 },
 {
   "STT": 11,
   "tencoso": " Viện Sốt rét - Ký sinh trùng - Côn trùng Quy Nhơn",
   "address": "611B Nguyễn Thái Học, Nguyễn Văn Cừ, Thành phố Qui Nhơn, Nguyễn Văn Cừ Thành phố Qui Nhơn Bình Định, Vietnam",
   "Longtitude": 13.7575418,
   "Latitude": 109.2080347
 },
 {
   "STT": 12,
   "tencoso": " Học viện Y - Dược học cổ truyền Việt Nam",
   "address": "2 Trần Phú, P. Mộ Lao, Hà Đông, Hà Nội, Vietnam",
   "Longtitude": 20.9847751,
   "Latitude": 105.7900592
 },
 {
   "STT": 13,
   "tencoso": " Viện Kiểm nghiệm thuốc Trung ương",
   "address": "48 Hai Bà Trưng, Tràng Tiền, Hoàn Kiếm, Hà Nội, Vietnam",
   "Longtitude": 20.9847715,
   "Latitude": 105.7572282
 },
 {
   "STT": 14,
   "tencoso": " Viện Kiểm nghiệm thuốc thành phố Hồ Chí Minh",
   "address": "200 Cô Bắc, Phường Cô Giang, Quận 1, Hồ Chí Minh, Vietnam",
   "Longtitude": 10.7641739,
   "Latitude": 106.6915899
 },
 {
   "STT": 15,
   "tencoso": " Viện Kiểm định Quốc gia vắc xin và Sinh phẩm y tế",
   "address": "Phường Đại Kim, Đại Kim, Hoàng Mai, Hà Nội, Vietnam",
   "Longtitude": 20.9722227,
   "Latitude": 105.8197776
 },
 {
   "STT": 16,
   "tencoso": " Viện Kiểm nghiệm an toàn vệ sinh thực phẩm Quốc gia",
   "address": "65 Phạm Thận Duật, Mai Dịch, Cầu Giấy, Hà Nội, Vietnam",
   "Longtitude": 21.0455974,
   "Latitude": 105.7761382
 },
 {
   "STT": 17,
   "tencoso": " Viện Dược liệu",
   "address": "3 Quang Trung, Tràng Tiền, Hoàn Kiếm, Hà Nội, Vietnam",
   "Longtitude": 21.0258244,
   "Latitude": 105.8477894
 },
 {
   "STT": 18,
   "tencoso": " Viện Trang thiết bị và Công trình y tế",
   "address": "40 Phương Mai, Hà Nội, Vietnam",
   "Longtitude": 21.0036993,
   "Latitude": 105.8345349
 },
 {
   "STT": 19,
   "tencoso": " Viện Vắc xin và Sinh phẩm y tế",
   "address": "Phường Đại Kim, Đại Kim, Hoàng Mai, Hà Nội, Vietnam",
   "Longtitude": 20.9722227,
   "Latitude": 105.8197776
 },
 {
   "STT": 20,
   "tencoso": " Viện Pháp y Quốc gia",
   "address": "41 Nguyễn Đình Chiểu, Lê Đại Hành, Hai Bà Trưng, Hà Nội, Vietnam",
   "Longtitude": 21.014203,
   "Latitude": 105.8447599
 },
 {
   "STT": 21,
   "tencoso": " Viện Pháp y tâm thần Trung ương",
   "address": "Hòa Bình, Thường Tín, Hanoi, Vietnam",
   "Longtitude": 20.8698102,
   "Latitude": 105.8410347
 }
];