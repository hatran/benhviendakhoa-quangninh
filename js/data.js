var data = [
 {
   "STT": 1,
   "Name": " Bệnh viện Bạch Mai",
   "address": "\n78 Đường Giải Phóng, Phương Đình, Đống Đa, Hà Nội, Vietnam",
   "Longtitude": 21.0009571,
   "Latitude": 105.8384967,
   "Rating": "3,98",
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 2,
   "Name": " Bệnh viện Chợ Rẫy",
   "address": "201B Nguyễn Chí Thanh, Phường 12, Quận 5, Hồ Chí Minh, Việt Nam",
   "Longtitude": 10.7576103,
   "Latitude": 10.7576103,
   "Rating": "3,98",
   "Number_of_beds": 700,
   "area": "Tỉnh"
 },
 {
   "STT": 3,
   "Name": " Bệnh viện Đa khoa Trung ương Huế",
   "address": "16 Lê Lợi, Vĩnh Ninh, Thành phố Huế, Thừa Thiên Huế, Vietnam",
   "Longtitude": 16.4624435,
   "Latitude": 107.5857696,
   "Rating": "3,98",
   "Number_of_beds": 500,
   "area": "Tỉnh"
 },
 {
   "STT": 4,
   "Name": " Bệnh viện Đa khoa Trung ương Thái Nguyên",
   "address": "479 Lương Ngọc Quyến, Phan Đình Phùng, Thành phố Thái Nguyên, Thái Nguyên, Vietnam",
   "Longtitude": 21.587921,
   "Latitude": 105.8287514,
   "Rating": "3,98",
   "Number_of_beds": 400,
   "area": "Tỉnh"
 },
 {
   "STT": 5,
   "Name": " Bệnh viện Đa khoa Trung ương Cần Thơ",
   "address": "315 Đường Nguyễn Văn Linh, Phường An Khánh, Ninh Kiều, Cần Thơ, Vietnam",
   "Longtitude": 10.0287684,
   "Latitude": 105.7532103,
   "Rating": "3,98",
   "Number_of_beds": 800,
   "area": "Tỉnh"
 },
 {
   "STT": 6,
   "Name": " Bệnh viện Đa khoa Trung ương Quảng Nam",
   "address": "Tam Hiệp, Núi Thành District, Quang Nam Province, Vietnam",
   "Longtitude": 15.4462831,
   "Latitude": 108.6202852,
   "Rating": "3,98",
   "Number_of_beds": 400,
   "area": "Tỉnh"
 },
 {
   "STT": 7,
   "Name": " Bệnh viện Việt Nam - Thụy Điển Uông Bí",
   "address": "Tuệ Tĩnh, Thanh Sơn, Thành phố Uông Bí, Quảng Ninh, Vietnam",
   "Longtitude": 21.0416532,
   "Latitude": 106.7498527,
   "Rating": "3,98",
   "Number_of_beds": 500,
   "area": "Tỉnh"
 },
 {
   "STT": 8,
   "Name": " Bệnh viện Hữu nghị Việt Nam - Cu Ba Đồng Hới",
   "address": "Tiểu khu 10, Thành phố Đồng Hới, Quang Binh Province, Vietnam",
   "Longtitude": 17.4740975,
   "Latitude": 106.6001879,
   "Rating": "3,98",
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 9,
   "Name": " Bệnh viện Hữu nghị Việt - Đức",
   "address": "40 Tràng Thi, Hàng Bông, Hoàn Kiếm, Hà Nội 100000, Vietnam",
   "Longtitude": 21.0284493,
   "Latitude": 105.8449,
   "Rating": "3,98",
   "Number_of_beds": 700,
   "area": "Tỉnh"
 },
 {
   "STT": 10,
   "Name": " Bệnh viện E",
   "address": "89 Trần Cung, Nghĩa Tân, Từ Liêm, Nghĩa Tân Cầu Giấy Hà Nội, Vietnam",
   "Longtitude": 21.0503064,
   "Latitude": 105.786389,
   "Rating": "3,98",
   "Number_of_beds": 800,
   "area": "Tỉnh"
 },
 {
   "STT": 11,
   "Name": " Bệnh viện Hữu nghị",
   "address": "1 Trần Khánh Dư, Bạch Đằng, Hai Bà Trưng, Hà Nội, Vietnam",
   "Longtitude": 21.0154664,
   "Latitude": 105.8596027,
   "Rating": "3,98",
   "Number_of_beds": 900,
   "area": "Tỉnh"
 },
 {
   "STT": 12,
   "Name": " Bệnh viện Thống Nhất",
   "address": "1 Lý Thường Kiệt, Phường 7, Tân Bình, Hồ Chí Minh 700000, Vietnam",
   "Longtitude": 10.7913517,
   "Latitude": 106.6512435,
   "Rating": "3,98",
   "Number_of_beds": 500,
   "area": "Tỉnh"
 },
 {
   "STT": 13,
   "Name": " Bệnh viện C Đà Nẵng",
   "address": "122 Hải Phòng, Thạch Thang, Hải Châu, Đà Nẵng 550000, Vietnam",
   "Longtitude": 16.072587,
   "Latitude": 108.2143862,
   "Rating": "3,98",
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 14,
   "Name": " Bệnh viện K",
   "address": "43 Quán Sứ, Hàng Bông, Hoàn Kiếm, Hà Nội 110000, Vietnam",
   "Longtitude": 20.9950723,
   "Latitude": 105.787429,
   "Rating": "3,98",
   "Number_of_beds": 700,
   "area": "Tỉnh"
 },
 {
   "STT": 15,
   "Name": " Bệnh viện Nhi Trung ương",
   "address": "18/879 Đường La Thành, Láng Thượng, Đống Đa, Hà Nội, Vietnam",
   "Longtitude": 21.0248819,
   "Latitude": 105.8053109,
   "Rating": "3,98",
   "Number_of_beds": 550,
   "area": "Tỉnh"
 },
 {
   "STT": 16,
   "Name": " Bệnh viện Phụ - Sản Trung ương",
   "address": "43 Tràng Thi, Hàng Bông, Hoàn Kiếm, Hà Nội, Vietnam",
   "Longtitude": 21.0271029,
   "Latitude": 105.8446325,
   "Rating": "3,98",
   "Number_of_beds": 650,
   "area": "Tỉnh"
 },
 {
   "STT": 17,
   "Name": " Bệnh viện Mắt Trung ương",
   "address": "85 Phố Bà Triệu, Bùi Thị Xuân, Hai Bà Trưng, Hà Nội, Vietnam",
   "Longtitude": 21.0176104,
   "Latitude": 105.8471562,
   "Rating": "3,98",
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 18,
   "Name": " Bệnh viện Tai - Mũi - Họng Trung ương",
   "address": "78 Giải Phóng, Phương Đình, Đống Đa, Hà Nội, Vietnam",
   "Longtitude": 20.9996628,
   "Latitude": 105.8384244,
   "Rating": "3,98",
   "Number_of_beds": 700,
   "area": "Tỉnh"
 },
 {
   "STT": 19,
   "Name": " Bệnh viện Nội tiết Trung ương CS1",
   "address": "ngõ 215 Ngọc Hồi-Tứ Hiệp-Thanh Trì, Hà Nội, Vietnam",
   "Longtitude": 20.9525937,
   "Latitude": 105.8486668,
   "Rating": "3,98",
   "Number_of_beds": 800,
   "area": "Tỉnh"
 },
 {
   "STT": 20,
   "Name": " Bệnh viện Nội tiết Trung ương CS2",
   "address": "80 Ngõ 82 Yên Lãng, Láng Hạ, Đống Đa, Hà Nội, Vietnam",
   "Longtitude": 21.0020122,
   "Latitude": 105.7853574,
   "Rating": "3,98",
   "Number_of_beds": 500,
   "area": "Tỉnh"
 },
 {
   "STT": 21,
   "Name": " Bệnh viện Răng - Hàm - Mặt Trung ương Hà Nội",
   "address": "40 Tràng Thi, Hàng Bông, Hoàn Kiếm, Hà Nội, Vietnam",
   "Longtitude": 21.0275643,
   "Latitude": 105.8287526,
   "Rating": "3,98",
   "Number_of_beds": 400,
   "area": "Tỉnh"
 },
 {
   "STT": 22,
   "Name": " Bệnh viện Răng - Hàm - Mặt Trung ương thành phố Hồ Chí Minh",
   "address": "201A Nguyễn Chí Thanh, Phường 12, Quận 5, Hồ Chí Minh, Vietnam",
   "Longtitude": 10.7579446,
   "Latitude": 106.6583029,
   "Rating": "3,98",
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 23,
   "Name": " Bệnh viện 71 Trung ương",
   "address": "thôn Tâm Trung, Thành phố Thanh Hóa, Thanh Hoa, Vietnam",
   "Longtitude": 19.7589178,
   "Latitude": 105.8398521,
   "Rating": "3,98",
   "Number_of_beds": 800,
   "area": "Tỉnh"
 },
 {
   "STT": 24,
   "Name": " Bệnh viện 74 Trung ương",
   "address": "Phường Hùng Vương - Thị xã Phúc Yên - Tỉnh Vĩnh Phúc, Phường Hùng Vương, Phúc Yên, Hùng Vương, Vietnam",
   "Longtitude": 21.2217822,
   "Latitude": 105.7073197,
   "Rating": "3,98",
   "Number_of_beds": 500,
   "area": "Tỉnh"
 },
 {
   "STT": 25,
   "Name": " Bệnh viện Phổi Trung ương",
   "address": "463 Hoàng Hoa Thám, Vĩnh Phú, Ba Đình, Hà Nội, Vietnam",
   "Longtitude": 21.0427111,
   "Latitude": 105.8111674,
   "Rating": "3,98",
   "Number_of_beds": 700,
   "area": "Tỉnh"
 },
 {
   "STT": 26,
   "Name": " Bệnh viện Tâm thần Trung ương 1",
   "address": "Xã Hòa Bình, Huyện Thường Tín, Hoà Binh, Thường Tín, Hà Nội, Vietnam",
   "Longtitude": 20.869724,
   "Latitude": 105.8416619,
   "Rating": "3,98",
   "Number_of_beds": 500,
   "area": "Tỉnh"
 },
 {
   "STT": 27,
   "Name": " Bệnh viện Tâm thần Trung ương 2",
   "address": "Đường Nguyễn Ái Quốc, KP7, P. Tân Phong, Tp Biên Hoà, tỉnh Đồng Nai, Tân Phong, Thành phố Biên Hòa, Đồng Nai, Vietnam",
   "Longtitude": 10.965651,
   "Latitude": 106.8438833,
   "Rating": "3,98",
   "Number_of_beds": 860,
   "area": "Tỉnh"
 },
 {
   "STT": 28,
   "Name": " Bệnh viện Phong - Da liễu Trung ương Quy Hòa",
   "address": "05A Chế Lan Viên, Ghềnh Ráng, Thành phố Qui Nhơn, Bình Định 591300, Vietnam",
   "Longtitude": 13.7503106,
   "Latitude": 109.2058564,
   "Rating": "3,98",
   "Number_of_beds": 750,
   "area": "Tỉnh"
 },
 {
   "STT": 29,
   "Name": " Bệnh viện Phong - Da liễu Trung ương Quỳnh Lập",
   "address": "Quỳnh Lập Huyện Quỳnh Lưu, Quỳnh Lập, Quỳnh Lưu, Nghệ An, Vietnam",
   "Longtitude": 19.260892,
   "Latitude": 105.7269458,
   "Rating": "3,98",
   "Number_of_beds": 560,
   "area": "Tỉnh"
 },
 {
   "STT": 30,
   "Name": " Bệnh viện Điều dưỡng - Phục hồi chức năng Trung ương",
   "address": "Đường Nguyễn Du, P. Bắc Sơn, Tp. Sầm Sơn, Thanh Hoá, Vietnam",
   "Longtitude": 19.7326347,
   "Latitude": 105.8953989,
   "Rating": "3,98",
   "Number_of_beds": 870,
   "area": "Tỉnh"
 },
 {
   "STT": 31,
   "Name": " Bệnh viện Bệnh Nhiệt đới Trung ương",
   "address": "78 Giải Phóng, Phương Đình, Đống Đa, Hà Nội, Vietnam",
   "Longtitude": 21.0010555,
   "Latitude": 105.8389524,
   "Rating": "3,98",
   "Number_of_beds": 750,
   "area": "Tỉnh"
 },
 {
   "STT": 32,
   "Name": " Bệnh viện Da liễu Trung ương",
   "address": "15A Phương Mai, Đống Đa, Hà Nội, Vietnam",
   "Longtitude": 21.0038047,
   "Latitude": 105.8365449,
   "Rating": "3,98",
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 33,
   "Name": " Bệnh viện Lão khoa Trung ương",
   "address": "1A Phương Mai, Đống Đa, Hà Nội, Vietnam",
   "Longtitude": 21.0037666,
   "Latitude": 105.8373619,
   "Rating": "3,98",
   "Number_of_beds": 760,
   "area": "Tỉnh"
 },
 {
   "STT": 34,
   "Name": " Bệnh viện Y học cổ truyền Trung ương",
   "address": "29 Nguyễn Bỉnh Khiêm, Nguyễn Du, Hai Bà Trưng, Hà Nội, Vietnam",
   "Longtitude": 21.0160969,
   "Latitude": 105.8464142,
   "Rating": "3,98",
   "Number_of_beds": 860,
   "area": "Tỉnh"
 },
 {
   "STT": 35,
   "Name": " Bệnh viện Châm cứu Trung ương",
   "address": "49 Thái Thịnh, Thịnh Quang, Hà Nội, Vietnam",
   "Longtitude": 21.0085992,
   "Latitude": 105.8171874,
   "Rating": "3,98",
   "Number_of_beds": 770,
   "area": "Tỉnh"
 }
];