var soyte = [
 {
   "STT": 1,
   "Name": "Sở y tế Thành phố Hà Nội",
   "address": "4 Sơn Tây, Điện Bàn, Ba Đình, Hà Nội, Vietnam",
   "Longtitude": 21.0330767,
   "Latitude": 105.8311022
 },
 {
   "STT": 2,
   "Name": "Sở y tế Tỉnh Hà Giang",
   "address": "số 338 Nguyễn Trãi, P. Nguyễn Trãi, Hà Giang, Vietnam",
   "Longtitude": 22.8213865,
   "Latitude": 104.9803171
 },
 {
   "STT": 3,
   "Name": "Sở y tế Tỉnh Cao Bằng",
   "address": "31 Đường Hiến Giang, P. Hợp giang, Cao Bằng, Vietnam",
   "Longtitude": 22.6669736,
   "Latitude": 106.2534212
 },
 {
   "STT": 4,
   "Name": "Sở y tế Tỉnh Bắc Kạn",
   "address": "14 Trường Chinh, Phùng Chí Kiên, Thành phố Bắc Kạn, Bắc Kạn, Vietnam",
   "Longtitude": 22.146672,
   "Latitude": 105.8330937
 },
 {
   "STT": 5,
   "Name": "Sở y tế Tỉnh Tuyên Quang",
   "address": "1 Đinh Tiên Hoàng, P. Tân Quang, Tuyên Quang, Vietnam",
   "Longtitude": 21.8180869,
   "Latitude": 105.2071689
 },
 {
   "STT": 6,
   "Name": "Sở y tế Tỉnh Lào Cai",
   "address": "Bình Minh, TX.Lào Cai, Lào Cai, Vietnam",
   "Longtitude": 22.4357086,
   "Latitude": 104.024773
 },
 {
   "STT": 7,
   "Name": "Sở y tế Tỉnh Điện Biên",
   "address": "Số 48 Tổ 25 Tôn Thất Tùng, Phường Mường Thanh, Thành phố Điện Biên Phủ, Điện Biên, Vietnam\n\n",
   "Longtitude": 21.383423,
   "Latitude": 103.0205637
 },
 {
   "STT": 8,
   "Name": "Sở y tế Tỉnh Lai Châu",
   "address": "245 Trần Phú, Tân Phong, Thị xã Lai Châu, Lai Châu, Vietnam",
   "Longtitude": 22.3968592,
   "Latitude": 103.4592551
 },
 {
   "STT": 9,
   "Name": "Sở y tế Tỉnh Sơn La",
   "address": "48 Lò Văn Giá, P. Chiềng Lề, Sơn La, Vietnam",
   "Longtitude": 21.3498866,
   "Latitude": 103.9012816
 },
 {
   "STT": 10,
   "Name": "Sở y tế Tỉnh Yên Bái",
   "address": "656 Yên Ninh, P.Yên Ninh, Thành phố Yên Bái, Yên Bái, Vietnam",
   "Longtitude": 21.7261042,
   "Latitude": 104.8879622
 },
 {
   "STT": 11,
   "Name": "Sở y tế Tỉnh Hoà Bình",
   "address": "98 Trần Hưng Đạo, Sủ Ngòi, Hòa Bình, Vietnam",
   "Longtitude": 20.8244577,
   "Latitude": 105.3527261
 },
 {
   "STT": 12,
   "Name": "Sở y tế Tỉnh Thái Nguyên",
   "address": "27 Bến Tượng, Trưng Vương, Thành phố Thái Nguyên, Thái Nguyên, Vietnam",
   "Longtitude": 21.5979387,
   "Latitude": 105.842477
 },
 {
   "STT": 13,
   "Name": "Sở y tế Tỉnh Lạng Sơn",
   "address": "Số 50 Đinh Tiên Hoàng, Chi Lăng, Thành phố Lạng Sơn, Lạng Sơn, Vietnam",
   "Longtitude": 21.8460769,
   "Latitude": 106.7506647
 },
 {
   "STT": 14,
   "Name": "Sở y tế Tỉnh Quảng Ninh",
   "address": "P. Bạch Đằng, Thành phố Hạ Long, Quảng Ninh, Vietnam",
   "Longtitude": 20.9613884,
   "Latitude": 107.0508397
 },
 {
   "STT": 15,
   "Name": "Sở y tế Tỉnh Bắc Giang",
   "address": "Đường Hùng Vương, Phường Ngô Quyền, Bắc Giang, Vietnam",
   "Longtitude": 21.2789822,
   "Latitude": 106.1967556
 },
 {
   "STT": 16,
   "Name": "Sở y tế Tỉnh Phú Thọ",
   "address": "Đường Trần Phú, Ph.Gia Cẩm, Thành phố Việt Trì, Phú Thọ, Vietnam",
   "Longtitude": 21.3193625,
   "Latitude": 105.3964706
 },
 {
   "STT": 17,
   "Name": "Sở y tế Tỉnh Vĩnh Phúc",
   "address": "Đống Đa, Vĩnh Yên, Vinh Phuc Province, Vietnam",
   "Longtitude": 21.3074566,
   "Latitude": 105.6046257
 },
 {
   "STT": 18,
   "Name": "Sở y tế Tỉnh Bắc Ninh",
   "address": "Đường Lý Thái Tổ, Suối Hoa, Bắc Ninh, Vietnam",
   "Longtitude": 21.1828655,
   "Latitude": 106.0729541
 },
 {
   "STT": 19,
   "Name": "Sở y tế Tỉnh Hải Dương",
   "address": "42 Quang Trung, P. Quang Trung, Thành phố Hải Dương, Hải Dương, Vietnam",
   "Longtitude": 20.8996159,
   "Latitude": 106.3686883
 },
 {
   "STT": 20,
   "Name": "Sở y tế Thành phố Hải Phòng",
   "address": "38 Lê Đại Hành, Hoàng Văn Thụ, Hồng Bàng, Hải Phòng 180000, Vietnam",
   "Longtitude": 20.8589975,
   "Latitude": 106.6815738
 },
 {
   "STT": 21,
   "Name": "Sở y tế Tỉnh Hưng Yên",
   "address": "Hải Thượng Lãn Ông, P. An Tảo, Hưng Yên, Vietnam",
   "Longtitude": 20.6650691,
   "Latitude": 106.059764
 },
 {
   "STT": 22,
   "Name": "Sở y tế Tỉnh Thái Bình",
   "address": "239 Hai Bà Trưng, P. Đề Thám, Thái Bình, Vietnam",
   "Longtitude": 20.444564,
   "Latitude": 106.3365868
 },
 {
   "STT": 23,
   "Name": "Sở y tế Tỉnh Hà Nam",
   "address": "Trường Chinh, Lương Khánh Thiện, tp. Phủ Lý, Hà Nam, Vietnam",
   "Longtitude": 20.5409005,
   "Latitude": 105.9155752
 },
 {
   "STT": 24,
   "Name": "Sở y tế Tỉnh Nam Định",
   "address": "14 Trần Nhân Tông, Thống Nhất, TP. Nam Định, Nam Định, Vietnam",
   "Longtitude": 20.4384217,
   "Latitude": 106.1749119
 },
 {
   "STT": 25,
   "Name": "Sở y tế Tỉnh Ninh Bình",
   "address": "18 Kim Đồng, Phú Thành, Ninh Bình, Vietnam",
   "Longtitude": 20.2520945,
   "Latitude": 105.9702905
 },
 {
   "STT": 26,
   "Name": "Sở y tế Tỉnh Thanh Hóa",
   "address": "101 Nguyễn Trãi, P. Ba Đình, Thành phố Thanh Hóa, Thanh Hoá, Vietnam",
   "Longtitude": 19.8031522,
   "Latitude": 105.7648611
 },
 {
   "STT": 27,
   "Name": "Sở y tế Tỉnh Nghệ An",
   "address": "18 Trường Thi, Thành phố Vinh, Nghệ An, Vietnam",
   "Longtitude": 18.670448,
   "Latitude": 105.6902683
 },
 {
   "STT": 28,
   "Name": "Sở y tế Tỉnh Hà Tĩnh",
   "address": "71 Hải Thượng Lãn Ông, Bắc Hà, Hà Tĩnh, Vietnam",
   "Longtitude": 18.3435301,
   "Latitude": 105.8927348
 },
 {
   "STT": 29,
   "Name": "Sở y tế Tỉnh Quảng Bình",
   "address": "2 Hồ Xuân Hương, Đồng Mỹ, Đồng Hới, Quảng Bình, Vietnam",
   "Longtitude": 17.4764284,
   "Latitude": 106.6193855
 },
 {
   "STT": 30,
   "Name": "Sở y tế Tỉnh Quảng Trị",
   "address": "34 Trần Hưng Đạo, Phường 1, Đông Hà, Quảng Trị, Vietnam",
   "Longtitude": 16.8214792,
   "Latitude": 107.0940479
 },
 {
   "STT": 31,
   "Name": "Sở y tế Tỉnh Thừa Thiên Huế",
   "address": "28 Lê Lợi, Vĩnh Ninh, Thành phố Huế, Vĩnh Ninh, Vietnam",
   "Longtitude": 16.4659074,
   "Latitude": 107.5870831
 },
 {
   "STT": 32,
   "Name": "Sở y tế Thành phố Đà Nẵng",
   "address": "103 Hùng Vương, Hải Châu 1, Q. Hải Châu, Đà Nẵng 550000, Vietnam",
   "Longtitude": 16.0378628,
   "Latitude": 108.2018069
 },
 {
   "STT": 33,
   "Name": "Sở y tế Tỉnh Quảng Nam",
   "address": "15 Trần Hung Đạo, Tam Kỳ, Phường Tân Thạnh, Tam Kỳ, Quảng Nam, Vietnam",
   "Longtitude": 15.5757992,
   "Latitude": 108.4728643
 },
 {
   "STT": 34,
   "Name": "Sở y tế Tỉnh Quảng Ngãi",
   "address": "19 Nguyễn Chánh, Trần Phú, Tp. Quảng Ngãi, Quảng Ngãi, Vietnam",
   "Longtitude": 15.120899,
   "Latitude": 108.7812986
 },
 {
   "STT": 35,
   "Name": "Sở y tế Tỉnh Bình Định",
   "address": "756 Trần Hưng Đạo, Đống Đa, Thành phố Qui Nhơn, Bình Định, Vietnam",
   "Longtitude": 13.7842948,
   "Latitude": 109.2123402
 },
 {
   "STT": 36,
   "Name": "Sở y tế Tỉnh Phú Yên",
   "address": "Tố Hữu, Phường 9, Tuy Hòa, Phú Yên, Vietnam",
   "Longtitude": 13.1065136,
   "Latitude": 109.3007369
 },
 {
   "STT": 37,
   "Name": "Sở y tế Tỉnh Khánh Hòa",
   "address": "4 Phan Chu Trinh, Xương Huân, Thành phố Nha Trang, Khánh Hòa 650000, Vietnam",
   "Longtitude": 12.254421,
   "Latitude": 109.1920752
 },
 {
   "STT": 38,
   "Name": "Sở y tế Tỉnh Ninh Thuận",
   "address": "1 21 Tháng 8, Mỹ Hương, Phan Rang-Tháp Chàm, Ninh Thuận, Vietnam",
   "Longtitude": 11.5673143,
   "Latitude": 108.9871035
 },
 {
   "STT": 39,
   "Name": "Sở y tế Tỉnh Bình Thuận",
   "address": "59 Lê Hồng Phong, Phú Trinh, Thành phố Phan Thiết, Bình Thuận",
   "Longtitude": 10.9307394,
   "Latitude": 108.1009315
 },
 {
   "STT": 40,
   "Name": "Sở y tế Tỉnh Kon Tum",
   "address": "71 Phan Đình Phùng, Quang Trung, Tp. Kon Tum, Kon Tum, Vietnam",
   "Longtitude": 14.3565882,
   "Latitude": 107.99773
 },
 {
   "STT": 41,
   "Name": "Sở y tế Tỉnh Gia Lai",
   "address": "09 Trần Hưng Đạo, P.Tây Sơn, Thành phố Pleiku, Tây Sơn 600000, Vietnam",
   "Longtitude": 13.9804745,
   "Latitude": 108.0015415
 },
 {
   "STT": 42,
   "Name": "Sở y tế Tỉnh Đắk Lắk",
   "address": "68 Lê Duẩn, Tân Thành, Thành phố Buôn Ma Thuột, Đắk Lắk, Vietnam",
   "Longtitude": 12.6682859,
   "Latitude": 108.0375445
 },
 {
   "STT": 43,
   "Name": "Sở y tế Tỉnh Đắk Nông",
   "address": "Trần Hưng Đạo, Phường Nghĩa Trung, Gia Nghĩa, Đăk Nông, Vietnam",
   "Longtitude": 12.0019662,
   "Latitude": 107.6880354
 },
 {
   "STT": 44,
   "Name": "Sở y tế Tỉnh Lâm Đồng",
   "address": "Tầng 2, Trung tâm hành chính tỉnh, Số 36 Trần Phú, TP. Đà Lạt ",
   "Longtitude": 11.936122,
   "Latitude": 108.4297176
 },
 {
   "STT": 45,
   "Name": "Sở y tế Tỉnh Bình Phước",
   "address": "1041 QL14, Tiến Thành, Đồng Xoài, Bình Phước, Vietnam",
   "Longtitude": 11.5275928,
   "Latitude": 106.8648743
 },
 {
   "STT": 46,
   "Name": "Sở y tế Tỉnh Tây Ninh",
   "address": "22 Lê Lợi, Phường 3, Tây Ninh, Vietnam",
   "Longtitude": 11.310422,
   "Latitude": 106.0982483
 },
 {
   "STT": 47,
   "Name": "Sở y tế Tỉnh Bình Dương",
   "address": "211 Yersin, Phú Chánh, Tp. Thủ Dầu Một, Phú Chánh Tân Uyên Bình Dương, Vietnam",
   "Longtitude": 11.0569219,
   "Latitude": 106.6801942
 },
 {
   "STT": 48,
   "Name": "Sở y tế Tỉnh Đồng Nai",
   "address": "2 Phan Đình Phùng, Quyết Thắng, Thành phố Biên Hòa, Đồng Nai, Vietnam",
   "Longtitude": 10.9485937,
   "Latitude": 106.8146623
 },
 {
   "STT": 49,
   "Name": "Sở y tế Tỉnh Bà Rịa - Vũng Tàu",
   "address": "so 1 p, Phạm Văn Đồng, Phước Trung, Bà Rịa, Bà Rịa - Vũng Tàu, Vietnam",
   "Longtitude": 10.4876681,
   "Latitude": 107.1784068
 },
 {
   "STT": 50,
   "Name": "Sở y tế Thành phố Hồ Chí Minh",
   "address": "59 Nguyễn Thị Minh Khai, Phường Bến Thành, Quận 1, Hồ Chí Minh, Vietnam",
   "Longtitude": 10.7740939,
   "Latitude": 106.6881748
 },
 {
   "STT": 51,
   "Name": "Sở y tế Tỉnh Long An",
   "address": "98 Nguyễn Đình Chiểu, Phường 1, Tân An, Long An, Vietnam",
   "Longtitude": 10.5387794,
   "Latitude": 106.4073707
 },
 {
   "STT": 52,
   "Name": "Sở y tế Tỉnh Tiền Giang",
   "address": "4 Hùng Vương, Phường 1, Thành phố Mỹ Tho, Phường 1, Vietnam",
   "Longtitude": 10.3563592,
   "Latitude": 106.3621802
 },
 {
   "STT": 53,
   "Name": "Sở y tế Tỉnh Bến Tre",
   "address": "39 Đoàn Hoàng Minh, Phường 5, Bến Tre, Vietnam",
   "Longtitude": 10.23865,
   "Latitude": 106.3658463
 },
 {
   "STT": 54,
   "Name": "Sở y tế Tỉnh Trà Vinh",
   "address": "16 Nguyễn Thái Học, Phường 1, Trà Vinh, Vietnam",
   "Longtitude": 9.9397634,
   "Latitude": 106.3375845
 },
 {
   "STT": 55,
   "Name": "Sở y tế Tỉnh Vĩnh Long",
   "address": "47 Lê Văn Tám, Phường 1, Vĩnh Long, Vietnam",
   "Longtitude": 10.2545047,
   "Latitude": 105.9671741
 },
 {
   "STT": 56,
   "Name": "Sở y tế Tỉnh Đồng Tháp",
   "address": "05 Võ Trường Toản, Phường 1, TP. Cao Lãnh, Đồng Tháp, Vietnam",
   "Longtitude": 10.4593458,
   "Latitude": 105.6309081
 },
 {
   "STT": 57,
   "Name": "Sở y tế Tỉnh An Giang",
   "address": "15 Lê Triệu Kiết, Mỹ Bình, tp. Long Xuyên Mỹ Bình tp. Long Xuyên An Giang, P. Mỹ Bình, Thành phố Long Xuyên, An Giang, Vietnam",
   "Longtitude": 10.3886678,
   "Latitude": 105.4344452
 },
 {
   "STT": 58,
   "Name": "Sở y tế Tỉnh Kiên Giang",
   "address": "1 Trần Hưng Đạo, Thanh Vân, Rạch Giá, tỉnh Kiên Giang, Vietnam",
   "Longtitude": 10.0083078,
   "Latitude": 105.0822956
 },
 {
   "STT": 59,
   "Name": "Sở y tế Thành phố Cần Thơ",
   "address": "71 Lý Tự Trọng, An Cư, Ninh Kiều, Cần Thơ, Vietnam",
   "Longtitude": 10.0353865,
   "Latitude": 105.7763474
 },
 {
   "STT": 60,
   "Name": "Sở y tế Tỉnh Hậu Giang",
   "address": "6 Ngô Quyền, Phường 5, Vị Thanh, Hậu Giang, Vietnam",
   "Longtitude": 9.7927331,
   "Latitude": 105.4805034
 },
 {
   "STT": 61,
   "Name": "Sở y tế Tỉnh Sóc Trăng",
   "address": "6 Châu Văn Tiếp, Phường 2, Sóc Trăng, Vietnam",
   "Longtitude": 9.5989695,
   "Latitude": 105.9693131
 },
 {
   "STT": 62,
   "Name": "Sở y tế Tỉnh Bạc Liêu",
   "address": "Nguyễn Tất Thành, Phường 1, Bạc Liêu, Vietnam",
   "Longtitude": 9.2883694,
   "Latitude": 105.7191878
 },
 {
   "STT": 63,
   "Name": "Sở y tế Tỉnh Cà Mau",
   "address": "155 Bùi Thị Trường, Phường 5, Thành phố Cà Mau, Cà Mau, Vietnam",
   "Longtitude": 9.1787229,
   "Latitude": 105.1543091
 }
];