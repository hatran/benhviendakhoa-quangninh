var datakhoidonvikhac = [
    {
      "STT": 1,
      "Name": "Chi cục dân số, kế hoạch hóa gia đình",
      "Latitude":  105.43692,
      "Rating": 2.98,
      "Longtitude": 10.3878345,
      "address": "số 13 Lê Triệu Kiết, phường Mỹ Bình, Long Xuyên, ",
      "Number_of_beds": "",
      "area": "Tỉnh"
},{
      "STT": 2,
      "Name": "Chi cục an toàn vệ sinh thực phẩm",
      "Latitude":  105.4408835,
      "Rating": 3.98,
      "Longtitude": 10.3845662,
      "address": "108 Lê Minh Ngươn, P. Mỹ Long, thành phố Long Xuyên, ",
      "Number_of_beds": "",
      "area": "Tỉnh"
},{
      "STT": 3,
      "Name": "Trung tâm y tế dự phòng",
      "Latitude":   105.4395485,
      "Rating": 4.98,
      "Longtitude": 10.3871257 ,
      "address": "12B, Lê Lợi, P.Mỹ Bình, thành phố Long Xuyên",
      "Number_of_beds": "",
      "area": "Tỉnh"
},{
      "STT": 4,
      "Name": "Trung tâm chăm sóc sức khỏe sinh sản",
      "Latitude":   105.4365098,
      "Rating": 1.98,
      "Longtitude": 10.385608,
      "address": "79 Tôn Đức Thắng, phường Mỹ Bình, thành phố Long Xuyên",
      "Number_of_beds": "",
      "area": "Tỉnh"
},{
      "STT": 5,
      "Name": "Trung tâm truyền thông giáo dục sức khỏe",
      "Latitude":  105.4368881,
      "Rating": 2.98,
      "Longtitude": 10.3906075,
      "address": "10-11 Lê Lợi, phường Mỹ Bình, thành phố Long Xuyên",
      "Number_of_beds": "",
      "area": "Tỉnh"
},{
      "STT": 6,
      "Name": "Trung tâm phòng chống HIV/AISD",
      "Latitude":  105.4403096,
      "Rating": 3.98,
      "Longtitude": 10.387413,
      "address": "Số 20 Nguyễn Du, Phường Mỹ Bình, thành phố Long Xuyên",
      "Number_of_beds": "",
      "area": "Tỉnh"

},{
      "STT": 7,
      "Name": "Trung tâm kiểm nghiệm dược - Mỹ phẩm",
      "Latitude": 105.4403096,
      "Rating": 4.98,
      "Longtitude": 10.387413,
      "address": "Số 20, Nguyễn Du - P. Mỹ Bình - thành phố Long Xuyên",
      "Number_of_beds": "",
      "area": "Tỉnh"

},{
      "STT": 8,
      "Name": "Trung tâm kiểm dịch y tế quốc tế",
      "Latitude": 105.4792378,
      "Rating": 4.98,
      "Longtitude": 10.3355273,
      "address": "Đường Phan Xích Long, P.Mỹ Thạnh, thành phố Long Xuyên",
      "Number_of_beds": "",
      "area": "Tỉnh"

},
{
      "STT": 9,
      "Name": "Trung tâm giám định y khoa",
      "Latitude": 105.4368881,
      "Rating": 5.98,
      "Longtitude": 10.3906075,
      "address": "10-11 Lê Lợi, phường Mỹ Bình, thành phố Long Xuyên",
      "Number_of_beds": "",
      "area": "Tỉnh"
}
,{
      "STT": 10,
      "Name": "Trung tâm pháp y",
      "Latitude": 105.4368881,
      "Rating": 5.98,
      "Longtitude": 10.3906075,
      "address": "10-11 Lê Lợi, phường Mỹ Bình, thành phố Long Xuyên",
      "Number_of_beds": "",
      "area": "Tỉnh"
}
];