var vucuc = [
 {
   "STT": 2,
   "Name": "Vụ Sức khỏe Bà mẹ - Trẻ em.",
   "address": "Kế Hoạch Hoá Gia Đình, Văn Miếu, Đống Đa, Hà Nội, Vietnam",
   "Longtitude": 21.0275935,
   "Latitude": 105.8341955
 },
 {
   "STT": 3,
   "Name": "Vụ Trang thiết bị và Công trình y tế.",
   "address": "40 Phương Mai, Hà Nội, Vietnam",
   "Longtitude": 21.0036993,
   "Latitude": 105.8345349
 },
 {
   "STT": 4,
   "Name": "Vụ Bảo hiểm y tế.",
   "address": "138A, Giảng Võ, Ba Đình, Hà Nội.",
   "Longtitude": 21.029424,
   "Latitude": 105.8247033
 },
 {
   "STT": 5,
   "Name": "Vụ Kế hoạch - Tài chính.",
   "address": "138A, Giảng Võ, Ba Đình, Hà Nội.",
   "Longtitude": 21.029424,
   "Latitude": 105.8247033
 },
 {
   "STT": 6,
   "Name": "Vụ Tổ chức cán bộ.",
   "address": "138A, Giảng Võ, Ba Đình, Hà Nội.",
   "Longtitude": 21.029424,
   "Latitude": 105.8247033
 },
 {
   "STT": 7,
   "Name": "Vụ Hợp tác quốc tế.",
   "address": "138A, Giảng Võ, Ba Đình, Hà Nội.",
   "Longtitude": 21.029424,
   "Latitude": 105.8247033
 },
 {
   "STT": 8,
   "Name": "Vụ Pháp chế.",
   "address": "138A, Giảng Võ, Ba Đình, Hà Nội.",
   "Longtitude": 21.029424,
   "Latitude": 105.8247033
 },
 {
   "STT": 9,
   "Name": "Cục Y tế dự phòng.",
   "address": "Ngõ 135, Đường Núi Trúc, Phường Giảng Võ, Quận Ba Đình, Kim Mã, Ba Đình, Hà Nội, Vietnam",
   "Longtitude": 21.0294785,
   "Latitude": 105.8219124
 },
 {
   "STT": 10,
   "Name": "Cục Phòng, chống HIV/AIDS.",
   "address": "Land 8 That Thuyet Street, Kim Mã, Ba Đình, Hà Nội, Vietnam",
   "Longtitude": 21.0307954,
   "Latitude": 105.8222226
 },
 {
   "STT": 11,
   "Name": "Cục An toàn thực phẩm.",
   "address": "138 Giảng Võ, Kim Mã, Ba Đình, Ba Đình Hà Nội, Vietnam",
   "Longtitude": 21.0307945,
   "Latitude": 105.8057459
 },
 {
   "STT": 12,
   "Name": "Cục Quản lý Môi trường y tế.",
   "address": "ngõ 8 Tôn Thất Thuyết, Dịch Vọng Hậu, Cầu Giấy, Hà Nội, Vietnam",
   "Longtitude": 21.0292939,
   "Latitude": 105.7797137
 },
 {
   "STT": 13,
   "Name": "Cục Khoa học công nghệ và Đào tạo.",
   "address": "138A Giảng Võ, Ba Đình, Hà Nội, Vietnam",
   "Longtitude": 21.0270768,
   "Latitude": 105.8209951
 },
 {
   "STT": 14,
   "Name": "Cục Quản lý Khám, chữa bệnh.",
   "address": "138A Giảng Võ, Ba Đình, Hà Nội, Vietnam",
   "Longtitude": 21.0270768,
   "Latitude": 105.8209951
 },
 {
   "STT": 15,
   "Name": "Cục Quản lý Y, Dược cổ truyền.",
   "address": "138A Giảng Võ - Ba Đình - Hà Nội",
   "Longtitude": 21.0270768,
   "Latitude": 105.8209951
 },
 {
   "STT": 16,
   "Name": "Cục Quản lý Dược.",
   "address": "138A Giảng Võ - Ba Đình - Hà Nội",
   "Longtitude": 21.0270768,
   "Latitude": 105.8209951
 },
 {
   "STT": 17,
   "Name": "Cục Công nghệ thông tin.",
   "address": " 135 Núi Trúc, Kim Mã, Ba Đình, Hà Nội",
   "Longtitude": 21.0294325,
   "Latitude": 105.8244384
 },
 {
   "STT": 18,
   "Name": "Tổng cục Dân số - Kế hoạch hóa gia đình.",
   "address": "Ngõ 8 Tôn Thất Thuyết,, Dịch Vọng Hậu, Nam Từ Liêm, Hà Nội, Vietnam",
   "Longtitude": 21.0289159,
   "Latitude": 105.7795213
 },
 {
   "STT": 19,
   "Name": "Văn Phòng Bộ",
   "address": "138A Giảng Võ, Kim Mã, Ba Đình, Hà Nội",
   "Longtitude": 21.0270768,
   "Latitude": 105.8209951
 },
 {
   "STT": 20,
   "Name": "Thanh Tra Bộ",
   "address": "138A Giảng Võ, Kim Mã, Ba Đình, Hà Nội",
   "Longtitude": 21.0270768,
   "Latitude": 105.8209951
 }
];