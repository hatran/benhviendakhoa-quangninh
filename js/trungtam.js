var trungtam = [
 {
   "STT": 1,
   "Name": " Trung tâm Truyền thông - Giáo dục sức khỏe Trung ương",
   "address": "366, Phố Đội Cấn, Phường Cống Vị, Quận Ba Đình, Thành Phố Hà Nội, Vĩnh Phú, Ba Đình, Hà Nội",
   "Longtitude": 21.0291572,
   "Latitude": 105.8236204
 },
 {
   "STT": 2,
   "Name": " Trung tâm Nghiên cứu, sản xuất vắc xin và Sinh phẩm y tế",
   "address": "135, Phố Lò Đúc, Phường Phạm Đình Hổ, Quận Hai Bà Trưng, Phạm Đình Hổ, Hai Bà Trưng, Hà Nội, Vietnam",
   "Longtitude": 21.0116797,
   "Latitude": 105.8572187
 },
 {
   "STT": 3,
   "Name": "  Trung tâm Điều phối Quốc gia về ghép bộ phận cơ thể người",
   "address": "40 Phố Tràng Thi - Hà Nội - Việt Nam.",
   "Longtitude": 21.0278086,
   "Latitude": 105.8442257
 },
];