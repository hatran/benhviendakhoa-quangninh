(function($) {

    "use strict";



    // backtop functions

    $( '.backtotop' ).on( 'click', function () {

      $( 'html, body' ).animate( { scrollTop: 0 }, 800 );

      return false;

    });

    $( window ).on( 'scroll', function () {

      if ( $( this ).scrollTop() > 200 ) {

        $( '.backtotop' ).fadeIn( 1000, function() {

          $( 'span' , this ).fadeIn( 100 );

        });

      } else {

        $( '.backtotop' ).fadeOut( 1000, function() {

          $( 'span' , this ).fadeOut( 100 );

        });

      }

    });

    $('.slider').slick({
      dots: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
    });

    $('.tintuc_slide').slick({
      dots: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
    });
    $(document).ready(function () {  
    $(window).bind("scroll", function(e) {
        var top = $(window).scrollTop();
        
      if (top> 740) { //Khoảng cách đã đo được
        $(".datlich").addClass("fix-box");
      } else {
        $(".datlich").removeClass("fix-box");
      } 
    });
});
})(jQuery);