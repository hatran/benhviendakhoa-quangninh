var truong = [
 {
   "STT": 1,
   "Name": " Trường Đại học Y Hà Nội",
   "address": "Số 1 Tôn Thất Tùng, Kim Liên, Đống Đa, Hà Nội 116001, Vietnam",
   "Longtitude": 21.0032249,
   "Latitude": 105.8284719
 },
 {
   "STT": 2,
   "Name": " Trường Đại học Dược Hà Nội",
   "address": "13-15 Lê Thánh Tông, Phan Chu Trinh, Hoàn Kiếm, Hà Nội, Vietnam",
   "Longtitude": 21.0210805,
   "Latitude": 105.8563522
 },
 {
   "STT": 3,
   "Name": " Đại học Y - Dược thành phố Hồ Chí Minh",
   "address": "217 Hồng Bàng, Phường 11, Quận 5, Hồ Chí Minh, Vietnam",
   "Longtitude": 10.7523246,
   "Latitude": 106.6662241
 },
 {
   "STT": 4,
   "Name": " Trường Đại học Y Dược Hải Phòng",
   "address": "72A Nguyễn Bỉnh Khiêm, Đằng Giang, Ngô Quyền, Ngô Quyền Hải Phòng, Vietnam",
   "Longtitude": 20.8428363,
   "Latitude": 106.6958454
 },
 {
   "STT": 5,
   "Name": " Trường Đại học Y Dược Thái Bình",
   "address": "P. Kỳ Bá, Thái Bình, Thai Binh, Vietnam",
   "Longtitude": 20.4426318,
   "Latitude": 106.3370028
 },
 {
   "STT": 6,
   "Name": " Trường Đại học Y - Dược Cần Thơ",
   "address": "An Khánh, Ninh Kiều, Cần Thơ, Vietnam",
   "Longtitude": 10.0349143,
   "Latitude": 105.7529576
 },
 {
   "STT": 7,
   "Name": " Trường Đại học Y tế công cộng",
   "address": "1A Đức Thắng, Phường Đức Thắng, Đông Ngạc, Bắc Từ Liêm, Hà Nội, Vietnam",
   "Longtitude": 21.082786,
   "Latitude": 105.7778123
 },
 {
   "STT": 8,
   "Name": " Trường Đại học Điều dưỡng Nam Định",
   "address": "257 Hàn Thuyên, Vị Xuyên, TP. Nam Định, Nam Định 420000, Vietnam",
   "Longtitude": 20.4374821,
   "Latitude": 106.1811211
 },
 {
   "STT": 9,
   "Name": " Trường Đại học Kỹ thuật y tế Hải Dương",
   "address": "1 Vũ Hựu, P. Thanh Bình, Thành phố Hải Dương, Hải Dương, Vietnam",
   "Longtitude": 20.939184,
   "Latitude": 6.3036156
 },
 {
   "STT": 10,
   "Name": " Trường Đại học Kỹ thuật Y - Dược Đà Nẵng",
   "address": "99 Hùng Vương, Hải Châu 1, Hải Châu, Đà Nẵng 550000, Vietnam",
   "Longtitude": 16.0674787,
   "Latitude": 108.2163878
 }
];